#!/bin/sh

LE_ARC_DIR="/etc/letsencrypt/archive"

SECRETS=$(docker secret ls | tail -n +2 | awk '{print $2}')

find $LE_ARC_DIR -name 'fullchain*.pem' -o -name 'privkey*.pem' | sed "s,$LE_ARC_DIR/,," | while read file
do
    base_file=$(basename $file .pem | sed -r 's/[0-9]+$//')
    version=$(basename $file .pem | sed -r 's/^.*?([0-9]+)$/\1/')
    secret_name="le_$(dirname $file)_$base_file.$version"
    if ! echo $SECRETS | grep -q $secret_name
    then
        echo "Creating secret $secret_name for $file"
        docker secret create --label letsencrypt $secret_name $LE_ARC_DIR/$file
    else
        echo "Secret $secret_name already exists"
    fi
done

# update services
for service in $(docker service ls -f "label=le_auto" -q)
do
    echo "Checking service $service"

    docker service inspect --format '{{json .Spec.TaskTemplate.ContainerSpec.Secrets}}' $service \
      | sed -r 's/("File":\{)/\n/g' \
      | sed -n -r '/Name/ s/"[^"]+":"([^"]+)"[^"]+/\1 /g;s/"Mode":([[:digit:]]+)[^[:alnum:]]*/\1 / p' \
      | ( while read target uid gid mode secret_id secret
    do
        base_secret=$(echo $secret | sed -r 's/\.[0-9]+$//')
        last_secret=$(docker secret ls | tail -n +2 | awk '/'$base_secret'/ {print $2}' | sort | tail -1)
        if [ $secret != $last_secret ]
        then
            echo "Updating $secret to $last_secret"
            CHANGES="--secret-rm $secret --secret-add source=$last_secret,target=$target,uid=$uid,gid=$gid,mode=$mode $CHANGES"
        fi
    done
    if [ -n "$CHANGES" ]
    then
        docker service update $CHANGES $service
    fi)
done