
# certbot with inwx plugin and cron for renewal check
based on 
 - https://github.com/oGGy990/certbot-dns-inwx
 - http://www.labouisse.com/how-to/2017/02/09/docker-secrets-with-lets-encrypt

Letsencrypt certbot with inwx plugin and cron, automatically checking for renewal and creating secrets from certificates in docker.

---

Docker must be in swarm mode
```sh
docker swarm init
```

---

## Start container or service
Start a container or service to make certbot check for renewal every 12 hours
```sh
  docker run --detach --restart=always --name certbot-container -v /var/run/docker.sock:/var/run/docker.sock raphpa/certbot-inwx
```

---
## Copy inwx configuration
Copy inwx.cfg with login credentials into container or mount it as secret to /etc/letsencrypt/inwx.cfg
```sh
  docker cp inwx.cfg certbot-container:/etc/letsencrypt/inwx.cfg
```


---
### inwx.cfg
The inwx configuration file should look as follows

> certbot_dns_inwx:dns_inwx_url                    = https://api.domrobot.com/xmlrpc/
> certbot_dns_inwx:dns_inwx_username       = username
> certbot_dns_inwx:dns_inwx_password        = password
> certbot_dns_inwx:dns_inwx_shared_secret = sharedsecret if two factor auth is used, else set to 'none'


---
## Create certificate
Create certificate with
```sh
  docker exec -it certbot-container certbot certonly --agree-tos --register-unsafely-without-email --server https://acme-v02.api.letsencrypt.org/directory -a certbot-dns-inwx:dns-inwx -d <domain.tld> -d <*.domain.tld>
```

Or create a test certificate on the staging environment with
```sh
  docker exec -it certbot-container certbot certonly --agree-tos --register-unsafely-without-email --staging -a certbot-dns-inwx:dns-inwx -d <domain.tld>
```

---
Secrets are automatically created from fullchain.pem and privkey.pem
Services using the renewed secrets are updated automatically if they have the label **'le_auto'** 

### docker-compose.yml
```sh
version: "3.6"
services:
  traefik:
    image: traefik:alpine
    deploy:
      replicas: 1
      restart_policy:
        condition: any
        delay: 5s
      placement:
        constraints:
          - node.role == manager
      labels:
        le_auto: "true"
...
```

---
## Shell access
The container shell can be accessed with
```sh
docker exec -it certbot-container /bin/sh
```