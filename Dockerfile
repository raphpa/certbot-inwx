FROM alpine:latest
LABEL maintainer="docker@pala.de"

# copy update script for docker secrets
COPY createsecret.sh /opt/createsecret.sh
COPY certbot /usr/sbin/certbot


# install docker and certbot with inwx plugin
RUN apk add --no-cache certbot docker && \
    apk add --no-cache --virtual .build-deps git && \
    git clone https://github.com/oGGy990/certbot-dns-inwx.git /opt/certbot-dns-inwx && \
    cd /opt/certbot-dns-inwx && \
    mkdir -p /etc/letsencrypt/ && \
    cp inwx.cfg /etc/letsencrypt/inwx.cfg  && \
    python setup.py develop --no-deps && \
    apk del --no-cache .build-deps && \
    chmod +rwx /usr/sbin/certbot

# create volume
VOLUME /etc/letsencrypt
    
# add cron job to check for renewal
RUN echo '0 */12 * * * /usr/sbin/certbot renew' | crontab -

# start cron in foreground
CMD [ "crond", "-f" ]
